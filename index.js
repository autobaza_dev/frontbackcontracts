const http = require("http");
const path = require("path");
const express = require("express");
const swaggerUi = require("swagger-ui-express");
const yamljs = require("yamljs");
const resolveRefs = require("json-refs").resolveRefs;

const multiFileSwagger = (root) => {
  const options = {
    filter: ["relative", "remote"],
    loaderOptions: {
      processContent: function (res, callback) {
        callback(null, yamljs.parse(res.text));
      },
    },
  };

  return resolveRefs(root, options).then(
    function (results) {
      return results.resolved;
    },
    function (err) {
      console.log(err.stack);
    }
  );
};

const createServer = async () => {
  const app = express();

  const swaggerDocument = await multiFileSwagger(
    yamljs.load(path.resolve(__dirname, "./api/swagger/swagger.yaml"))
  );

  app.use("/", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  const server = http.createServer(app);

  return server;
};

createServer()
  .then((server) => {
    const port = 3005;
    server.listen(port);
  })
  .catch((err) => {
    console.error(err.stack);
    process.exit(1);
  });
