const gulp = require("gulp");
const browserSync = require("browser-sync").create();

//Converts yaml to json
gulp.task("swagger", done => {
  browserSync.reload();
  done();
});

//Watches for changes
gulp.task("watch", function() {
  gulp.watch("api/swagger/**/*.yaml", gulp.series("swagger","bs-reload", "browser-sync"));
});

gulp.task("serve", function() {
  browserSync.init({
    proxy: "http://localhost:3005"
  });
  gulp.watch("api/swagger/**/*.yaml", gulp.series("swagger"));
});

gulp.task("default", gulp.series("serve"));
